public class Food
{
    private String description;
    private int calories;
    
    public void setCalories(int inCalories)
    {
        calories = inCalories;
    }
    public int getCalories()
    {
        return calories;
    }
    
    
    /**
     * The getter (or accessor) for the description member
     * @return The description
     */
    public String getDescription()
    {
        return description;
    }
    /**
    * Set the description
    *@param inDescription is the new description
    */
    public void setDescription(String inDescription)
    {
        description = inDescription;
    }
    public String toString();
    {
        return "Somebody brought " + getDescription();
    }
    
}